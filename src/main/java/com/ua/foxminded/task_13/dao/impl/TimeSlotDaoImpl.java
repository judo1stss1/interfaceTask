package com.ua.foxminded.task_13.dao.impl;

import com.ua.foxminded.task_13.dao.DaoEntity;
import com.ua.foxminded.task_13.model.TimeSlot;
import com.ua.foxminded.task_13.model.mapper.TimeSLotMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;

import java.util.List;

@Repository
public class TimeSlotDaoImpl implements DaoEntity<TimeSlot> {


    private final JdbcTemplate jdbcTemplate;

    private static final String SQL_FIND_TIMESLOT = "select * from time_slots where timeslot_id = ?";
    private static final String SQL_DELETE_TIMESLOT = "delete from time_slots where timeslot_id = ?";
    private static final String SQL_UPDATE_TIMESLOT = "update time_slots set start_lesson = ?, end_lesson = ?, group_id= ?, lesson_id= ? where timeslot_id = ?";
    private static final String SQL_GET_ALL_TIMESLOT = "select * from time_slots";
    private static final String SQL_INSERT_TIMESLOT = "insert into time_slots(start_lesson, end_lesson, group_id, lesson_id) values(?,?,?,?)";

    @Autowired
    public TimeSlotDaoImpl(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public TimeSlot getById(Long id) {
        return jdbcTemplate.queryForObject(SQL_FIND_TIMESLOT, new Object[]{id}, new TimeSLotMapper());
    }

    @Override
    public List<TimeSlot> getAll() {
        return jdbcTemplate.query(SQL_GET_ALL_TIMESLOT, new TimeSLotMapper());
    }

    @Override
    public boolean delete(Long id) {
        return jdbcTemplate.update(SQL_DELETE_TIMESLOT, id) > 0;
    }

    @Override
    public boolean update(TimeSlot timeSlot) {
        return jdbcTemplate.update(SQL_UPDATE_TIMESLOT, timeSlot.getStartLesson(), timeSlot.getEndLesson(),
                timeSlot.getTimeSlotId()) > 0;
    }

    @Override
    public boolean create(TimeSlot timeSlot) {
        return jdbcTemplate.update(SQL_INSERT_TIMESLOT, timeSlot.getStartLesson(), timeSlot.getEndLesson()) > 0;
    }

}
